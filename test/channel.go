package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func printFish(fishChan chan bool, dogChan chan bool) {
	defer wg.Done()
	defer close(fishChan)

	for i := 0;i < 3; i++ {
		<-fishChan
		fmt.Println("fish")
		dogChan <- true
	}
}

func printDog(dogChan chan bool, catChan chan bool) {
	defer wg.Done()
	defer close(dogChan)

	for i := 0;i < 3; i++ {
		<-dogChan
		fmt.Println("dog")
		catChan <- true
	}
}

func printCat(catChan chan bool, fishChan chan bool) {
	defer wg.Done()
	defer close(catChan)

	for i := 0;i < 3; i++ {
		<-catChan
		fmt.Println("cat")
		if i != 2 {
			fishChan <- true
		}
	}
}


func main() {
	// 循环打印fish，dog, cat
	fishChan := make(chan bool, 1)
	dogChan := make(chan bool, 1)
	catChan := make(chan bool, 1)
	fishChan <- true

	go printFish(fishChan, dogChan)
	go printDog(dogChan, catChan)
	go printCat(catChan, fishChan)

	wg.Add(3)
	wg.Wait()
}