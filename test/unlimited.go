package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	var jsonStr = "{\"operator\":\"and\",\"condition\":[{\"operator\":\"and\",\"condition\":[{\"fieldName\":\"channel\",\"operator\":\"eq\",\"fieldValue\":\"命理\"},{\"fieldName\":\"content_id\",\"operator\":\"eq\",\"fieldValue\":\"news_calendar\"}]},{\"operator\":\"and\",\"condition\":[{\"fieldName\":\"source_id\",\"operator\":\"eq\",\"fieldValue\":\"22\"}]}]}"
	result := map[string]interface{} {}

	err := json.Unmarshal([]byte(jsonStr), &result)

	fmt.Println(err)
	fmt.Printf("#+v", result)
}