package main

import "fmt"

func main() {

	c  := make(chan struct{})
	for i := 0; i < 10; i++ {
		go func() {
			for {
				select {
				case <-c:
					fmt.Println(111)
				}
			}
		}()
	}

	for {
		c <- struct{}{}
	}
}