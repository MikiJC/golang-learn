package leetcode

func merge(nums1 []int, m int, nums2 []int, n int) {
	i := m + n - 1
	x, y := m-1, n-1
	for x >= 0 && y >= 0 {
		if nums1[x] > nums2[y] {
			nums1[i] = nums1[x]
			i, x = i-1, x-1
		} else {
			nums1[i] = nums2[y]
			i, y = i-1, y-1
		}
	}

	for y >= 0 {
		nums1[i] = nums2[y]
		i, y = i-1, y-1
	}
}
