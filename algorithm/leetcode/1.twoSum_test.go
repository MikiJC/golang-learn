package leetcode

import (
	"fmt"
	"reflect"
	"testing"
)

type input struct {
	slice []int
	num   int
}

func TestTwoSum(t *testing.T) {
	tests := []struct {
		input  input
		result []int
	}{
		{
			input: input{
				slice: []int{2, 7, 11, 15},
				num:   9,
			},
			result: []int{0, 1},
		},
		{
			input: input{
				slice: []int{3, 2, 4},
				num:   6,
			},
			result: []int{1, 2},
		},
	}

	for _, item := range tests {
		if got := TwoSum2(item.input.slice, item.input.num); !reflect.DeepEqual(got, item.result) {
			fmt.Println(got, item.result)
		}
	}
}
