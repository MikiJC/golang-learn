package leetcode

import (
	"fmt"
	"testing"
)

func TestTrap1(t *testing.T) {
	tests := []struct {
		input  []int
		result int
	}{
		//{
		//	input:  []int{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1},
		//	result: 6,
		//},
		//{
		//	input:  []int{4, 2, 0, 3, 2, 5},
		//	result: 9,
		//},
		{
			input:  []int{4, 2, 3},
			result: 1,
		},
	}

	for _, test := range tests {
		if got := Trap3(test.input); got != test.result {
			fmt.Printf("Trap1(%q) = %v", test.input, got)
		}
	}
}
