package leetcode

import (
	"fmt"
	"reflect"
	"testing"
)

func TestPlusOne(t *testing.T) {
	tests := []struct {
		input  []int
		result []int
	}{
		{
			input:  []int{9, 9, 9},
			result: []int{1, 0, 0, 0},
		},
	}

	for _, test := range tests {
		if got := PlusOne(test.input); reflect.DeepEqual(got, test.result) {
			fmt.Sprintf("RemoveDuplicates(%q) = %v", test.input, got)
		}
	}
}
