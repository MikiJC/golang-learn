package leetcode

func TwoSum(nums []int, target int) []int {
	ahash := map[int]int{}

	for k, v := range nums {
		ahash[v] = k
	}

	for k, v := range nums {
		if i, ok := ahash[target-v]; ok && i != k {
			return []int{k, i}
		}
	}
	return []int{}
}

func TwoSum2(nums []int, target int) []int {
	ahash := map[int]int{}

	for k, v := range nums {
		if i, ok := ahash[target-v]; ok && i != k {
			return []int{i, k}
		}
		ahash[v] = k
	}

	return []int{}
}
