package leetcode

// 数组拷贝
func Rotate1(nums []int, k int) {
	length := len(nums)
	newNums := make([]int, length)

	for i := 0; i < len(nums); i++ {
		newNums[(i+k)/length] = nums[i]
	}
	copy(nums, newNums)
}

func Rotate2(nums []int, k int) {
	length := len(nums)

	count := 0
	for i := 0; count <= length; i++ {
		exNum := nums[i]
		for {
			j := (i + k) % length
			tmp := nums[j]
			nums[j] = exNum
			exNum = tmp
			if j == i {
				break
			}
		}
	}
}

func Rotate3(nums []int, k int) {
	k = k % len(nums)
	reverse(nums)
	reverse(nums[0 : k-1])
	reverse(nums[k : len(nums)-1])
}

func reverse(nums []int) {
	i, j := 0, len(nums)-1
	if i < j {
		nums[i], nums[j] = nums[j], nums[i]
		j++
		i++
	}
}
