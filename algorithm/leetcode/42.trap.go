package leetcode

// 动态规划
func Trap1(height []int) int {
	left, right := make([]int, len(height)), make([]int, len(height))

	tmp := 0
	for i := 0; i < len(height); i++ {
		if height[i] > tmp {
			tmp = height[i]
		}
		left[i] = tmp
	}

	tmp = 0
	for i := len(height) - 1; i >= 0; i-- {
		if height[i] > tmp {
			tmp = height[i]
		}
		right[i] = tmp
	}

	res := 0
	for i := 0; i < len(height); i++ {
		res += min(left[i], right[i]) - height[i]
	}
	return res
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a int, b int) int {
	if a > b {
		return b
	}
	return a
}

// 单调栈
func Trap2(height []int) int {
	stack := []int{}
	res := 0
	for i, h := range height {
		var current, left int
		for len(stack) > 0 && h > height[stack[len(stack)-1]] {
			current, stack = stack[len(stack)-1], stack[:len(stack)-1]
			if len(stack) == 0 {
				break
			}
			left = stack[len(stack)-1]
			res += (i - left - 1) * (min(height[left], h) - height[current])
		}
		stack = append(stack, i)
	}
	return res
}

// 双指针
func Trap3(height []int) int {
	left, right, leftMax, rightMax, res := 0, len(height)-1, 0, 0, 0
	for left < right {
		if height[left] < height[right] {
			if height[left] > leftMax {
				leftMax = height[left]
			} else {
				res += leftMax - height[left]
			}
			left++
		} else {
			if height[right] > rightMax {
				rightMax = height[right]
			} else {
				res += rightMax - height[right]
			}
			right--
		}
	}
	return res
}
