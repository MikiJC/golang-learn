package leetcode

func RemoveDuplicates(nums []int) int {
	pointA := 0

	if len(nums) == 0 {
		return 0
	}

	for pointB := 1; pointB < len(nums); pointB++ {
		if nums[pointA] != nums[pointB] {
			pointA++
			nums[pointA] = nums[pointB]
		}
	}

	return pointA + 1
}
