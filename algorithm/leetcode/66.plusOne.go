package leetcode

func PlusOne(digits []int) []int {
	for i := len(digits) - 1; i >= 0; i-- {
		digits[i] = (digits[i] + 1) % 10

		if digits[i] != 0 {
			break
		}
	}

	if digits[0] == 0 {
		res := []int{1}
		res = append(res, digits...)
		return res
	}
	return digits
}
