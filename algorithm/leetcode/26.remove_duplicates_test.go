package leetcode

import (
	"fmt"
	"testing"
)

func TestRemoveDuplicates(t *testing.T) {
	tests := []struct {
		input  []int
		result int
	}{
		{
			input:  []int{1},
			result: 1,
		},
		{
			input:  []int{},
			result: 0,
		},
		{
			input:  []int{1, 1},
			result: 1,
		},
		{
			input:  []int{1, 2},
			result: 2,
		},
		{
			input:  []int{0, 0, 1, 1, 1, 2, 2, 3, 3, 4},
			result: 5,
		},
	}

	for _, test := range tests {
		if got := RemoveDuplicates(test.input); got != test.result {
			fmt.Errorf("RemoveDuplicates(%q) = %v", test.input, got)
		}
	}

}
