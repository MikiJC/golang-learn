package leetcode

func moveZeroes(nums []int) {
	i := 0
	for j := 0; j < len(nums); j++ {
		if nums[j] != 0 {
			nums[i], i = nums[j], i+1
		}
	}

	for i < len(nums) {
		nums[i], i = 0, i+1
	}
}
