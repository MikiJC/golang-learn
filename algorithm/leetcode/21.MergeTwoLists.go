package leetcode

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	i, j := l1, l2
	p1 := &ListNode{
		Val:  0,
		Next: l1,
	}
	for i != nil && j != nil {
		if i.Val > j.Val {
			tmp := &ListNode{
				Val:  j.Val,
				Next: i,
			}
			p1.Next = tmp

			j, p1 = j.Next, p1.Next
		} else {
			p1, i = i, i.Next
		}
	}

	if i == nil && j != nil {
		i = j
	}
	return i
}
