package week1

// todo 没弄懂为什么一定要tmp变量
func Rotate(nums []int, k int)  {
	l := len(nums)
	k = k % l

	for startIndex, count := 0, 0; count < l; startIndex++ {
		aIndex, bIndex,tmp := startIndex, (startIndex + k) % l,nums[startIndex]
		for {
			m := tmp
			nums[bIndex], tmp = m, nums[bIndex]
			aIndex, bIndex, count = bIndex, (bIndex + k) % l, count+1
			if aIndex <= startIndex {
				break
			}
		}
	}
}

func Rotate1(nums []int, k int)  {
	l := len(nums)
	k = k % l

	swap(nums, 0, l -1)
	swap(nums, 0, k -1)
	swap(nums, k, l -1)
}

func swap(nums []int, startIndex int, endIndex int) {
	for startIndex <endIndex {
		nums[startIndex], nums[endIndex] = nums[endIndex], nums[startIndex]
		startIndex++
		endIndex--
	}
}
