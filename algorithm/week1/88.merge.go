package week1

func Merge(nums1 []int, m int, nums2 []int, n int)  {
	maxIndex := 0
	// m/n终止条件
	for maxIndex = m + n - 1; m > 0 && n > 0; maxIndex-- {
		if nums1[m - 1] < nums2[n - 1] {
			nums1[maxIndex] = nums2[n - 1]
			n--
		} else {
			nums1[maxIndex] = nums1[m - 1]
			m--
		}
	}

	// maxIndex的边界
	if maxIndex >= 0 && n > 0 {
		for n > 0 {
			nums1[maxIndex] = nums2[n - 1]
			n--
			maxIndex--
		}
	}
}
