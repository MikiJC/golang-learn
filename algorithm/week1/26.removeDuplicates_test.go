package week1

import (
	"fmt"
	"testing"
)

func TestRemoveDuplicates(t *testing.T) {
	input := []int{0,0,1,1,1,2,2,3,3,4}
	fmt.Println(RemoveDuplicates(input), input)
}