package week1

import "practice/algorithm"

func MergeTwoLists(list1 *algorithm.ListNode, list2 *algorithm.ListNode) *algorithm.ListNode {
	prePoint := &algorithm.ListNode{
		Val:  0,
		Next: list1,
	}
	point1, point2 := list1, list2
	for point1 != nil && point2 != nil {
		if point1.Val < point2.Val {
			point1 = point1.Next
		} else {
			prePoint.Next = &algorithm.ListNode{
				Val:  point2.Val,
				Next: point1,
			}
			point2 = point2.Next
		}
		prePoint = prePoint.Next
	}

	if point1 == nil && point2 != nil {
		prePoint.Next = point2
	}

	return list1
}

func MergeTwoLists2(list1 *algorithm.ListNode, list2 *algorithm.ListNode) *algorithm.ListNode {
	if list1 == nil {
		return list2
	}
	if list2 == nil {
		return list1
	}

	if list1.Val < list2.Val {
		list1.Next = MergeTwoLists2(list1.Next, list2)
		return list1
	} else {
		list2.Next = MergeTwoLists2(list1, list2.Next)
		return list2
	}
}