package week1

func PlusOne(digits []int) []int {
	for i := len(digits) - 1; i >= 0; i-- {
		if digits[i] == 9 {
			digits[i] = 0
		} else {
			digits[i]++
			return digits
		}
	}

	if len(digits) > 0 &&digits[0] == 0 {
		res := []int{1}
		return append(res, digits...)
	}
	return digits
}
