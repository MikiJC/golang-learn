package week1

import (
	"fmt"
	"testing"
)

func TestRotate(t *testing.T) {
	input := []int{1,2,3,4,5,6,7}
	k := 3
	Rotate(input, k)
	fmt.Println(input)
}


func TestRotate2(t *testing.T) {
	input := []int{1,2,3,4,5,6,7}
	k := 3
	Rotate1(input, k)
	fmt.Println(input)
}