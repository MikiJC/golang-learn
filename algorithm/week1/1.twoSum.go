package week1

func TwoSum(nums []int, target int) []int {
	hash := map[int]int{}
	for key, num := range nums {
		hash[num] = key
	}

	for key, num := range nums {
		num2 := target - num

		if key2, ok := hash[num2]; ok && key != key2 {
			return []int{key, key2}
		}
	}
	return []int{}
}
