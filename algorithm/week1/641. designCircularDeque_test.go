package week1

import (
	"fmt"
	"testing"
)

func TestConstructor(t *testing.T) {
	//["MyCircularDeque","insertFront","insertLast","insertLast","getFront","deleteLast","getRear","insertFront","deleteFront","getRear","insertLast","isFull"]
	//[[3],[8],[8],[2],[],[],[],[9],[],[],[2],[]]
	myCircularDeque := Constructor(3)
	fmt.Println(myCircularDeque.InsertFront(8), "InsertFront")  // return True
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.InsertLast(8), "InsertLast")  // return True
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.InsertLast(2), "InsertLast") // return True
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.GetFront(), "GetFront") // return False, the queue is full.
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.DeleteLast(), "DeleteLast")      // return 2
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.GetRear(), "GetRear")       // return True
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.InsertFront(9), "InsertFront")   // return True
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.DeleteFront(), "DeleteFront") // return True
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.GetRear(), "GetRear")     // return 4
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.InsertLast(2), "InsertLast")     // return 4
	myCircularDeque.Print()
	fmt.Println(myCircularDeque.IsFull(), "IsFull")     // return 4
	myCircularDeque.Print()
}