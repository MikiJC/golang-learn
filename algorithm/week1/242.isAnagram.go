package week1

func isAnagram(s string, t string) bool {
	hashMap := map[rune]int{}
	for _, item := range s {
		hashMap[item]++
	}

	for _, item := range t {
		hashMap[item]--
	}

	for _, num := range hashMap {
		if num != 0 {
			return false
		}
	}
	return true
}