package week1

import (
	"fmt"
	"testing"
)

func TestMerge(t *testing.T) {
	input1 := []int{1,2,3,0,0,0}
	input2 := []int{2,5,6}
	Merge(input1, 3, input2, 3)
	fmt.Println(input1)
}
