package week1

import "fmt"

type MyCircularDeque struct {
	arr []int
	length int
	max int
	start int
	end int
}


func Constructor(k int) MyCircularDeque {
	return MyCircularDeque{
		arr:    make([]int, k),
		max: k,
		length: 0,
		start:  0,
		end:    0,
	}
}


func (this *MyCircularDeque) InsertFront(value int) bool {
	if this.IsFull() {
		return false
	}
	this.arr[this.start] = value
	this.start = (this.start + 1)%this.max
	this.length++
	return true
}


func (this *MyCircularDeque) InsertLast(value int) bool {
	if this.IsFull() {
		return false
	}
	this.end = (this.end + this.max - 1) %this.max
	this.arr[this.end] = value
	this.length++
	return true
}


func (this *MyCircularDeque) DeleteFront() bool {
	if this.IsEmpty() {
		return false
	}
	this.start = (this.start + this.max - 1) %this.max
	this.arr[this.start] = 0
	this.length--
	return true
}


func (this *MyCircularDeque) DeleteLast() bool {
	if this.IsEmpty() {
		return false
	}
	this.arr[this.end] = 0
	this.end = (this.end + 1) %this.max
	this.length--
	return true
}


func (this *MyCircularDeque) GetFront() int {
	if this.IsEmpty() {
		return -1
	}
	return this.arr[(this.start + this.max - 1)%this.max]
}


func (this *MyCircularDeque) GetRear() int {
	if this.IsEmpty() {
		return -1
	}
	return this.arr[this.end]
}


func (this *MyCircularDeque) IsEmpty() bool {
	return this.length == 0
}


func (this *MyCircularDeque) IsFull() bool {
	return this.length == this.max
}

func (this *MyCircularDeque) Print() {
	fmt.Println(this.arr, this.start, this.end)
}


/**
 * Your MyCircularDeque object will be instantiated and called as such:
 * obj := Constructor(k);
 * param_1 := obj.InsertFront(value);
 * param_2 := obj.InsertLast(value);
 * param_3 := obj.DeleteFront();
 * param_4 := obj.DeleteLast();
 * param_5 := obj.GetFront();
 * param_6 := obj.GetRear();
 * param_7 := obj.IsEmpty();
 * param_8 := obj.IsFull();
 */