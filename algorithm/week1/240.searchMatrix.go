package week1

func searchMatrix(matrix [][]int, target int) bool {
	length := len(matrix)

	if length == 0 {
		return false
	}

	height := len(matrix[0])

	for m, n := 0, height - 1; m < length && n >= 0; {
		if matrix[m][n] > target {
			n--
		} else if matrix[m][n] < target {
			m++
		} else {
			return true
		}
	}
	return false
}
