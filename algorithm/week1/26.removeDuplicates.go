package week1

func RemoveDuplicates(nums []int) int {
	numMap := map[int]int{}
	res := 0

	for key, num := range nums {
		if numMap[num] == 0 {
			nums[res] = nums[key]
			numMap[num] = 1
			res++
		}
	}

	return res
}